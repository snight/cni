# CNI
CNI（Container Network Interface）是CNCF旗下的一个项目，由一组用于配置Linux容器的网络接口的规范和库组成，同时还包含了一些插件。CNI仅关心容器创建时的网络分配，和当容器被删除时释放网络资源。

# 下面介绍两种网络插件，Flannel和Calico，两者选一种安装即可。

# 第一种：安装网络插件Flannel
## 一、下载cni插件
```
git clone https://gitlab.com/snight/cni.git
```
## 二、配置kube-flannel.yml文件
替换默认的pod网段地址，例如这里将原来的10.244.0.0/16网段地址替换成为172.31.0.0/16
```
$ sed -i "s|10.244.0.0/16|172.31.0.0/16|" cni/kube-flannel.yml
```
flannel 默认会使用主机的第一张物理网卡，如果你有多张网卡，需要单独指定：
```shell
$ vim kube-flannel.yml
```
```yaml
containers:
- name: kube-flannel
  # image: quay.io/coreos/flannel:v0.13.1-rc1
  image: registry.cn-qingdao.aliyuncs.com/k8s-group/flannel:v0.13.1-rc1
  command:
  - /opt/bin/flanneld
  args:
  - --ip-masq
  - --kube-subnet-mgr
  # 添加物理网卡接口名称
  - --iface=ens192
```
为了能够正常完成部署，yaml资源文件中的镜像地址已经替换成为阿里云仓库的地址。
## 三、应用kube-flannel.yml资源文件
```shell
$ kubectl apply -f cni/kube-flannel.yml
```
## 四、查看pod状态
正常情况下有几个k8s节点就会看到几个名称为flannel的POD
```
$ kubectl get pod -n kubesystem | grep flannel
NAMESPACE     NAME                                       READY   STATUS    RESTARTS   AGE
kube-system   kube-flannel-ds-amd64-6qbhp                1/1     Running   0          37m
kube-system   kube-flannel-ds-amd64-7xghf                1/1     Running   0          51m
kube-system   kube-flannel-ds-amd64-m2t7z                1/1     Running   0          159m
kube-system   kube-flannel-ds-amd64-m2zbb                1/1     Running   0          37m
kube-system   kube-flannel-ds-amd64-mvvp5                1/1     Running   0          159m
kube-system   kube-flannel-ds-amd64-x57fp                1/1     Running   0          37m
kube-system   kube-flannel-ds-amd64-zczxk                1/1     Running   0          106m
```

# 第二种：安装网络插件Calico
## 一、安装calico前的准备工作
### 1、配置NetworkManager
NetworkManager会控制默认network命名空间中接口的路由表，这可能会干扰Calico代理的正确路由能力。
创建以下配置文件，以防止NetworkManager干扰接口：
```shell
$ cat > /etc/NetworkManager/conf.d/calico.conf << EOF
[keyfile]
unmanaged-devices=interface-name:cali*;interface-name:tunl*;interface-name:vxlan.calico
EOF
```
### 2、增加Linux的conntrack表空间的大小
Linux系统上的一个常见问题是conntrack表空间不足，这可能会导致iptables性能下降。如果在特定主机上工作负载过大，或者创建大量TCP连接或双向UDP数据流，则可能会发生这种情况。为避免此问题，我们建议使用以下命令增加conntrack表的大小：
```shell
sysctl -w net.netfilter.nf_conntrack_max=1000000
echo "net.netfilter.nf_conntrack_max=1000000" >> /etc/sysctl.conf
```
## 二、在节点上安装Calico
从Kubernetes安装Calico（以不超过50个节点为例）
### 1、下载Calico的yaml文件。
```
git clone https://gitlab.com/snight/cni.git
```
提示：本项目YAML文件中的镜像已经更换为阿里云的镜像仓库地址。

如果您使用的是pod CIDR `192.168.0.0/16`，请跳至下一步。如果您在kubeadm上使用了其他的Pod CIDR，则无需进行任何更改。Calico将根据正在运行的配置自动检测CIDR。对于其他平台，请确保在yaml文件中取消对`CALICO_IPV4POOL_CIDR`变量的注释，并将其设置为与所使用Pod CIDR相同的地址。
```shell
vim  cni/calico.yaml
```
```yaml
# The default IPv4 pool to create on startup if none exists. Pod IPs will be
# chosen from this range. Changing this value after installation will have
# no effect. This should fall within `--cluster-cidr`.
# - name: CALICO_IPV4POOL_CIDR
#   value: "192.168.0.0/16"
# Set MTU for the Wireguard tunnel device.
```
### 2、更新应用资源
```
kubectl apply -f cni/calico.yaml
```
## 三、calico网络插件的配置
### 1、启/禁用 ip-ip
目前官方提供的模板里，默认打开了`ip-ip`功能，该功能会在node上创建一个设备：`tunl0`，容器的网络数据会经过该设备被封装一个ip头再转发。这里，calico.yaml中通过修改`calico-node`的环境变量：`CALICO_IPV4POOL_IPIP`来实现ipip功能的开关：默认是`Always`，表示开启；`Off`表示关闭ipip; `cross-subnet`表示开启并支持跨子网，目前用不到这种类型。
```
- name: CALICO_IPV4POOL_IPIP
  value: "always"
```
### 2、ip识别策略
部署前，要配置一个参数，让calico-node组件能够识别node的IP，node上可能有多块网卡，官方提供的yaml文件中，ip识别策略（IPDETECTMETHOD）没有配置，即默认为first-found，这会导致一个网络异常的ip作为nodeIP被注册，从而影响node-to-node mesh。我们可以修改成can-reach的策略，使用您的本地路由来确定将使用哪个IP地址。这里我们一般使用物理网卡的网关地址或者为master节点的内网IP，否则会导致创建的tunl0无法连通，从而导致跨节点的POD无法通信等问题。
```yaml
# Cluster type to identify the deployment type
# 假设实验环境的网关地址为10.10.50.1
- name: CLUSTER_TYPE
  value: "k8s,bgp"
- name: IP_AUTODETECTION_METHOD
  value: "can-reach=10.10.50.1"
```
或者使用提供的接口正则表达式（go语法）枚举匹配的接口，并返回第一个匹配的接口的IP地址
```yaml
# Cluster type to identify the deployment type
- name: CLUSTER_TYPE
  value: "k8s,bgp"
- name: IP_AUTODETECTION_METHOD
  value: "interface=ens.*"
```
## 四、删除Calico的方法
### 1、删除calico对象
```
kubectl delete -f calico.yaml
```
### 2、检查所有节点上的网络，看看是否存在Tunl0，有则删除
```shell
# 查看
ip addr show
# 删除
modprobe -r ipip
```
### 3、移除Calico配置文件
```shell
rm -rf /etc/cni/net.d
```
